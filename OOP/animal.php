<?php

class Animal
{
  public $name;
  public $legs = 4;
  public $cold_blooded = "no";
  public function __construct($nama)
  {
    $this->name = $nama;
  }

  public function get_info()
  {
    echo "Name : " . $this->name . "<br>";
    echo "Legs : " . $this->legs . "<br>";
    echo "cold blooded : " . $this->cold_blooded . "<br>";
  }
}
