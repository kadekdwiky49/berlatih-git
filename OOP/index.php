<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo $sheep->get_info() . "<br>";

$kodok = new Frog("buduk");
echo $kodok->get_info();
echo "Yell : " . $kodok->jump() . "<br><br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->get_info();
echo "Yell : " . $sungokong->yell() . "<br><br>";



// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())